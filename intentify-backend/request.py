import requests

url = 'http://localhost:5000/codesearch'

# Set the natural language input query in the request body
data = {'query': 'find code to sort a list of integers'}

# Make the POST request
response = requests.post(url, json=data)

# Print the response
print(response.json())
